import React from 'react';
import {StyleSheet, Text, View, Image, ActivityIndicator} from 'react-native';
import axios from 'axios';

export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      loading: false,
      error: false,
      weather: [
        {
          main: null,
          icon: null,
          description: null
        }
      ],
      main: {
        temp: null, 
        pressure: null, 
        humidity: null, 
        temp_min: null, 
        temp_max: null, 
      },
      wind: {
        speed: null,
      },
      name: null
    }
  }
  componentDidMount(){
    this.setState({loading: true});
    axios.get("http://api.openweathermap.org/data/2.5/weather?q=Belo%20Horizonte,Br&appid=7489ef6eb644acdd47a1ce5776d531bd&units=metric").then(response => {
      console.log(response.data);
      this.setState({
        weather: [
          {
            main: response.data.weather[0].main,
            icon: 'http://openweathermap.org/img/wn/' + response.data.weather[0].icon + '@2x.png',
            description: response.data.weather[0].description
          }
        ],
        main: {
          temp: parseInt(response.data.main.temp) + 'º',
          pressure: response.data.main.pressure,
          humidity: response.data.main.humidity,
          temp_min: response.data.main.temp_min + 'º',
          temp_max: response.data.main.temp_max + 'º'
        },
        wind: response.data.wind,
        name: response.data.name,
        loading: false, 
        error: false
      });
    }).catch(error => {
      console.log(error);
      this.setState({loading: false, error: true});
    });
  }

  render() {
    if(this.state.loading){
      return (
        <View style={styles.container}>
          <ActivityIndicator size='large' color='#17202A' />
        </View>
      );
    }else if(this.state.error == false){
      return (
        <View style={styles.container}>
          <View style={styles.mainContainer}>
            <Text style={styles.mainTemp}>
              {this.state.main.temp}
            </Text>
            <View style={styles.weatherContainer}>
              <View style={styles.weatherMainContainer}>
                <Image source={{uri: this.state.weather[0].icon}} style={styles.icon} />
                <Text style={styles.weatherMain}>
                  {this.state.weather[0].main}
                </Text>
              </View>
              <Text style={styles.weatherDescription}>
                {this.state.weather[0].description}
              </Text>
            </View>
          </View>
        </View>
      );
    }else{
      return (
        <View style={styles.container}>
          <Text>
            Error!!!
          </Text>
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#AED6F1',
    padding: 16
  },
  mainContainer: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  mainTemp: {
    fontSize: 70,
    fontWeight: '100',
    color: '#17202A'
  },
  weatherContainer: {
    marginLeft: 16
  },
  weatherMain: {
    fontWeight: '900',
    fontSize: 20,
    color: '#17202A',
    textTransform: 'uppercase'
  },
  weatherDescription: {
    fontWeight: '100',
    fontSize: 17,
    color: '#566573',
    textTransform: 'uppercase'
  },
  icon: {
    height: 30, 
    width: 30
  },
  weatherMainContainer: {
    flexDirection: 'row', 
    alignItems: 'center'
  }
});
